var btn1 = document.querySelector(".btn1")
var btn2 = document.querySelector(".btn2")
var nota = []
var cont = -1
var storage = document.querySelector(".storage")
var calc = document.querySelector(".med")
var final = document.createElement('p')

function med() {
    var final = 0
    if (cont > 0) {
        for (i = 0; i < cont + 1; i++) {
            final += parseInt(nota[i])
        }
        final = final / i
    } else if (cont == 0) {
        final = parseInt(nota[0])
    }
    return final.toFixed(2)
}

btn1.addEventListener("click", () => {
    valor = document.querySelector(".nota").value
    if (valor == '') {
        alert('Por favor, insira uma nota')
    }
    else if (valor <= 10 && valor >= 0) {
        nota.push(valor)
        cont++
        var div = document.createElement('div')
        div.innerText = 'A nota ' + (cont + 1) + ' foi ' + nota[cont]
        storage.append(div)
    } else if (typeof valor == 'string') {
        alert('A nota digitada é inválida, por favor, insira uma nota válida')
    }
})

btn2.addEventListener("click", () => {
    final.innerText = med()
    calc.append(final)
})